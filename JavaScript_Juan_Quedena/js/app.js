var Calculadora = (function() {

    var _display;
    var entrada = "0";
    var ultimoNumero = "0";
    var salida = "";
    var operacion = "";
    var operadores = {
        "mas": "+",
        "menos": "-",
        "por": "*",
        "dividido": "/"
    }

    function inicializar(teclas, display) {
        _display = display;
        for(var i in teclas) {
            var tecla = teclas[i];
            tecla.onmouseup = soltarBoton;
            tecla.onmousedown = presionarBoton;
            tecla.onclick = clickBoton;
        }
        entrada = "0";
        salida = "";
        operacion = "";
        ultimoNumero = "";
    }

    function presionarBoton(e) {
        e.target.style.transform = "scale(0.9)";
    }

    function soltarBoton(e) {
        e.target.style.transform = "scale(1)";
    }

    function clickBoton(e) {
        var id = e.target.id;
        salida = "";
        if(id === "on") {
            entrada = "0";
            operacion = "";
        } else if(id === "dividido" 
            || id === "por" 
            || id === "menos" 
            || id === "mas"
            || id === "igual") {
            obtenerNumero(id);
        } else if(id === "sign") {
            if(entrada !== "0") {
                if(entrada.length === 0 && operacion.length > 0) {
                    entrada = operacion;
                }
                entrada = entrada.charAt(0) == "-" ? entrada.substr(1, entrada.length) : "-" + entrada;
            }
        } else if(id === "raiz") {
            console.log("No Implementado");
        } else if(id === "punto") {
            digitarNumero(".");
        } else {
            digitarNumero(id);
        }
        _display.innerHTML = entrada + salida;
    }

    function digitarNumero(numero) {
        numero = numero === "." && entrada.indexOf(".") > -1 ? "" : numero;
        if(numero !== "0" && entrada === "0") {
            entrada = numero === "." ? "0." :numero;
        } else if(!(numero === "0" && entrada === "0")) {
            entrada += entrada.length > (entrada.indexOf(".") > -1 ? 8 : 7)  ? "" : numero;
        }
    }

    function obtenerNumero(id) {
        if(id !== "igual") {
            operacion += entrada + operadores[id];
        } else {
            if(entrada.length > 0) {
                ultimoNumero = operacion.charAt(operacion.length - 1) + entrada;
            } else {
                entrada = ultimoNumero;
            }
            operacion += entrada;
            console.log(operacion);
            operacion = "" + eval(operacion);
            console.log("="+operacion);
            let pos = operacion.indexOf(".");
            let len = (operacion.charAt(0) === "-" ? 9 : 8)
            if(pos > -1) {
                salida = operacion.substr(0, operacion.length > len ? len : operacion.length);
            } else {
                salida = operacion.length > len ? "Overflow" : operacion;
            }
        }
        entrada = "";
    }

    return {
      presionarBoton: presionarBoton,
      soltarBoton: soltarBoton,
      clickBoton: clickBoton,
      inicializar: inicializar
    };
})();

var teclas = document.getElementsByClassName("tecla");
var display = document.getElementById("display");
Calculadora.inicializar(teclas, display);